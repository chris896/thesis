import numpy as np
import pandas as pd
from time import time
import keras
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint, EarlyStopping
from gensim.models import KeyedVectors
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
SEED = 2000

# read-set data
csv = 'thesis/Twitter_Sentiment_Medium/clean_tweet.csv'
my_df = pd.read_csv(csv,index_col=0)
my_df.head()

my_df.dropna(inplace=True)
my_df.reset_index(drop=True,inplace=True)
my_df.info()

x = my_df.text
y = my_df.target

# split train-validation-test data (98%/1%/1%)
x_train, x_val, y_train, y_val = train_test_split\
    (x, y, test_size=.02, random_state=SEED)



print("Train set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_train),
                                                                             (len(x_train[y_train == 0]) / (len(x_train)*1.))*100,
                                                                            (len(x_train[y_train == 1]) / (len(x_train)*1.))*100))
print("Validation set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_val),
                                                                             (len(x_val[y_val == 0]) / (len(x_val)*1.))*100,
                                                                            (len(x_val[y_val == 1]) / (len(x_val)*1.))*100))


#model_ug_cbow = KeyedVectors.load('w2v_model_ug_cbow_M.word2vec')
#model_ug_sg = KeyedVectors.load('w2v_model_ug_sg_M.word2vec')
#model_w = KeyedVectors.load_word2vec_format('wiki-news-300d-1M.vec', binary=False, encoding='utf-8')
model_g = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True, encoding='utf-8')

# num_words = the maximum number of words to keep, based on word frequency.
# Only the most common words will be kept.
# chosen after experimentation
input_shape = 45
num_words = 100000
tokenizer = Tokenizer(num_words=num_words)
tokenizer.fit_on_texts(x_train)
sequences = tokenizer.texts_to_sequences(x_train)
sequences_val = tokenizer.texts_to_sequences(x_val)
#num_words = len(tokenizer.word_index) + 1

# By padding the inputs, we decide the maximum length of words
# in a sentence, then zero pads the rest
x_train_seq = pad_sequences(sequences, maxlen=input_shape)
x_val_seq = pad_sequences(sequences_val, maxlen=input_shape)


embed_dim = 300
lstm_out = 300
batch_size = 32

embeddings_index = {}
#for w in model_ug_cbow.wv.vocab.keys():
#   embeddings_index[w] = np.append(model_ug_cbow.wv[w], model_ug_sg.wv[w])  # concat two models
for w in model_g.wv.vocab.keys():
  embeddings_index[w] = model_g.wv[w]
print('Found %s word vectors.' % len(embeddings_index))  # 13934 word vectors

# a matrix of weights only for words we will see during training
embedding_matrix = np.zeros((num_words,embed_dim))
for word, i in tokenizer.word_index.items():
    if i >= num_words:
        continue
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector

## One visible GPU - multiple experiments running at the same time
import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"
from keras import backend as K
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
K.set_session(sess)

np.random.seed(SEED)
model = Sequential()
model.add(Embedding(num_words, embed_dim, weights=[embedding_matrix],input_length = x_train_seq.shape[1], trainable=True))
#model.add(Embedding(num_words, embed_dim,input_length = x_train_seq.shape[1], trainable=True))
model.add(SpatialDropout1D(0.4))
model.add(LSTM(lstm_out, dropout=0.4, recurrent_dropout=0.4))
#model.add(Dense(256,activation='relu'))
#model.add(Dense(64,activation='relu'))
model.add(Dense(1,activation='sigmoid'))
model.compile(loss = 'binary_crossentropy', optimizer='adam',metrics = ['accuracy'])
print(model.summary())
# train model
t0 = time()
history = model.fit(x_train_seq, y_train, epochs=10, batch_size=batch_size, verbose=2, validation_data=(x_val_seq, y_val))
print ("-" * 80)

import matplotlib.pyplot as plt
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

score = model.evaluate(x=x_val_seq, y=y_val) # [val_loss, val_acc]
pred = []
for x in range(len(x_val_seq)):
    result=model.predict(x_val_seq[x].reshape(1, x_val_seq.shape[1]), verbose=2)
    p = np.argmax(result)
    pred.append(p)

prec = precision_score(y_val,pred, average=None)
rec = recall_score(y_val,pred, average=None)
f1 = f1_score(y_val,pred, average=None)
train_test_time = time() - t0

print ("\n")
print("Best validation [loss,accuracy] :")
print(score)
print("Train and test time: {0} sec".format(train_test_time))

print ("\n")
print("Precision [Sadness, Joy]: ")
print(prec)

print ("\n")
print("Recall [Sadness, Joy]: ")
print(rec)

print ("\n")
print("F1_score [Sadness, Joy]: ")
print(f1)

'''

'''