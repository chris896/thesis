# -*- coding: utf-8 -*-
import warnings
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')
import os
from sklearn import utils
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from tqdm import tqdm
tqdm.pandas(desc="progress-bar")
import keras
from keras import backend as K
from keras.models import Sequential
from keras.layers import Dense
from keras.layers.embeddings import Embedding
from keras.layers import Conv1D, GlobalMaxPooling1D
from gensim.models.word2vec import Word2Vec
from gensim.models.doc2vec import TaggedDocument
import multiprocessing
from gensim.models import KeyedVectors
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from time import time
SEED = 2000

# read-set data
csv = 'clean_tweet.csv'
my_df = pd.read_csv(csv,index_col=0)
my_df.head()

my_df.dropna(inplace=True)
my_df.reset_index(drop=True,inplace=True)
my_df.info()


x = my_df.text
y = my_df.target

# split train-validation-test data (98%/1%/1%)
x_train, x_val, y_train, y_val = train_test_split\
    (x, y, test_size=.02, random_state=SEED)



print("Train set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_train),
                                                                             (len(x_train[y_train == 0]) / (len(x_train)*1.))*100,
                                                                            (len(x_train[y_train == 1]) / (len(x_train)*1.))*100))
print("Validation set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_val),
                                                                             (len(x_val[y_val == 0]) / (len(x_val)*1.))*100,
                                                                            (len(x_val[y_val == 1]) / (len(x_val)*1.))*100))



'''
                ## Gensim's models training ##

## Labelise each tweet with unique IDs using Gensim’s LabeledSentence function
def labelize_tweets_ug(tweets,label):
    result = []
    prefix = label
    for i, t in zip(tweets.index, tweets):
        result.append(TaggedDocument(t.split(), [prefix + '_%s' % i]))
    return result

all_x = pd.concat([x_train,x_val])
all_x_w2v = labelize_tweets_ug(all_x, 'all')

#CBOW
cores = multiprocessing.cpu_count()
model_ug_cbow = Word2Vec(sg=0, size=100, negative=5, window=2, min_count=2, workers=cores, alpha=0.065, min_alpha=0.065)
model_ug_cbow.build_vocab([x.words for x in tqdm(all_x_w2v)])

st_time = time()
# train Word2Vec model
for epoch in range(30):
    model_ug_cbow.train(utils.shuffle([x.words for x in tqdm(all_x_w2v)]), total_examples=len(all_x_w2v), epochs=1)
    model_ug_cbow.alpha -= 0.002
    model_ug_cbow.min_alpha = model_ug_cbow.alpha

# SKIP-GRAM
model_ug_sg = Word2Vec(sg=1, size=100, negative=5, window=2, min_count=2, workers=cores, alpha=0.065, min_alpha=0.065)
model_ug_sg.build_vocab([x.words for x in tqdm(all_x_w2v)])

for epoch in range(30):
    model_ug_sg.train(utils.shuffle([x.words for x in tqdm(all_x_w2v)]), total_examples=len(all_x_w2v), epochs=1)
    model_ug_sg.alpha -= 0.002
    model_ug_sg.min_alpha = model_ug_sg.alpha

w2v_train_time = time() - st_time
print(w2v_train_time, 'sec')

# save models to .word2vec files
model_ug_cbow.save('w2v_model_ug_cbow_M.word2vec')
model_ug_sg.save('w2v_model_ug_sg_M.word2vec')
'''

model_ug_cbow = KeyedVectors.load('w2v_model_ug_cbow_M.word2vec')
model_ug_sg = KeyedVectors.load('w2v_model_ug_sg_M.word2vec')
#model_g = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True, encoding='utf-8')
#model_w = KeyedVectors.load_word2vec_format('wiki-news-300d-1M.vec', binary=False, encoding='utf-8')
#len(model_ug_cbow.wv.vocab.keys())

embeddings_index = {}
for w in model_ug_cbow.wv.vocab.keys():
   embeddings_index[w] = np.append(model_ug_cbow.wv[w], model_ug_sg.wv[w])  # concat two models
#for w in model_g.wv.vocab.keys():
#   embeddings_index[w] = model_g.wv[w]
#print('Found %s word vectors.' % len(embeddings_index))  # word vectors

# num_words = the maximum number of words to keep, based on word frequency.
# Only the most common words will be kept.
input_shape = 45
num_words = 100000
tokenizer = Tokenizer(num_words=num_words)
tokenizer.fit_on_texts(x_train)
sequences = tokenizer.texts_to_sequences(x_train)
#num_words = len(tokenizer.word_index) + 1

dim = 200
x_train_seq = pad_sequences(sequences, maxlen=input_shape)
print('Shape of data tensor:', x_train_seq.shape)

sequences_val = tokenizer.texts_to_sequences(x_val)
x_val_seq = pad_sequences(sequences_val, maxlen=input_shape)


embedding_matrix = np.zeros((num_words, dim))
for word, i in tokenizer.word_index.items():
    if i >= num_words:
        continue
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector


import os
os.environ["CUDA_VISIBLE_DEVICES"]="0"
from keras import backend as K
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
K.set_session(sess)


from keras.callbacks import ModelCheckpoint, EarlyStopping

filepath="CNN_best_weights.{epoch:02d}-{val_acc:.4f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
early_stop = EarlyStopping(monitor='val_acc', patience=5, mode='max') 
callbacks_list = [checkpoint, early_stop]

np.random.seed(SEED)
model_cnn_03 = Sequential()
e = Embedding(num_words, dim, weights=[embedding_matrix], input_length=input_shape, trainable=True)
#e = Embedding(num_words, dim, input_length=input_shape, trainable=True)
model_cnn_03.add(e)
model_cnn_03.add(Conv1D(filters=200, kernel_size=4, padding='valid', activation='relu', strides=1))
model_cnn_03.add(Conv1D(filters=200, kernel_size=4, padding='valid', activation='relu', strides=1))
model_cnn_03.add(Conv1D(filters=200, kernel_size=4, padding='valid', activation='relu', strides=1))
model_cnn_03.add(GlobalMaxPooling1D())
model_cnn_03.add(Dense(256, activation='tanh'))
model_cnn_03.add(Dense(2, activation='tanh'))
model_cnn_03.add(Dense(1, activation='sigmoid'))
model_cnn_03.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model_cnn_03.summary())

t0 = time()
history = model_cnn_03.fit(x_train_seq, y_train, validation_data=(x_val_seq, y_val), epochs=10, batch_size=32, verbose=2, callbacks=callbacks_list)
print ("-" * 80)

import matplotlib.pyplot as plt
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

score = model_cnn_03.evaluate(x=x_val_seq, y=y_val) # [val_loss, val_acc]
pred = []
for x in range(len(x_val_seq)):
    result = model_cnn_03.predict(x_val_seq[x].reshape(1, x_val_seq.shape[1]), verbose=2)
    p = np.argmax(result)
    pred.append(p)

prec = precision_score(y_val,pred, average=None)
rec = recall_score(y_val,pred, average=None)
f1 = f1_score(y_val,pred, average=None)
train_test_time = time() - t0

print ("\n")
print("Best validation [loss,accuracy] :")
print(score)
print("Train and test time: {0} sec".format(train_test_time))

print ("\n")
print("Precision [Sadness, Joy]: ")
print(prec)

print ("\n")
print("Recall [Sadness, Joy]: ")
print(rec)

print ("\n")
print("F1_score [Sadness, Joy]: ")
print(f1)

'''


'''
