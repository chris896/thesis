import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')

#matplotlib inline
#config InlineBackend.figure_format = 'retina'

cols = ['sentiment','id','date','query_string','user','text']
df = pd.read_csv("data_folder/training.1600000.processed.noemoticon.csv",header=None, names=cols,encoding='ISO-8859-1')
df.head()
df.info()
df.sentiment.value_counts()
df.query_string.value_counts()
df.drop(['id','date','query_string','user'],axis=1,inplace=True)
df.head()
df[df.sentiment == 0].head(10)
df[df.sentiment == 4].head(10)
df[df.sentiment == 0].index
df[df.sentiment == 4].index
df['sentiment'] = df['sentiment'].map({0: 0, 4: 1})
df.sentiment.value_counts()

from pprint import pprint
data_dict = {
    'sentiment':{
        'type':df.sentiment.dtype,
        'description':'sentiment class - 0:negative, 1:positive'
    },
    'text':{
        'type':df.text.dtype,
        'description':'tweet text'
    },
    'pre_clean_len':{
        'type':df.pre_clean_len.dtype,
        'description':'Length of the tweet before cleaning'
    },
    'dataset_shape':df.shape
}

pprint(data_dict)
fig, ax = plt.subplots(figsize=(5, 5))
plt.boxplot(df.pre_clean_len)
plt.show()
df[df.pre_clean_len > 140].head(10)

from bs4 import BeautifulSoup
import re
from nltk.tokenize import WordPunctTokenizer
tok = WordPunctTokenizer()

pat1 = r'@[A-Za-z0-9]+'
pat2 = r'https?://[A-Za-z0-9./]+'
combined_pat = r'|'.join((pat1, pat2))

def tweet_cleaner(text):
    soup = BeautifulSoup(text, 'lxml')
    souped = soup.get_text()
    stripped = re.sub(combined_pat, '', souped)
    try:
        clean = stripped.decode("utf-8-sig").replace(u"\ufffd", "?")
    except:
        clean = stripped
    letters_only = re.sub("[^a-zA-Z]", " ", clean)
    lower_case = letters_only.lower()
    # During the letters_only process two lines above, it has created unnecessay white spaces,
    # I will tokenize and join together to remove unneccessary white spaces
    words = tok.tokenize(lower_case)
    return (" ".join(words)).strip()

testing = df.text[:100]

test_result = []
for t in testing:
    test_result.append(tweet_cleaner(t))

test_result

nums = [0,400000,800000,1200000,1600000]

##time
print("Cleaning and parsing the tweets...\n")
clean_tweet_texts = []
for i in range(nums[0],nums[1]):
    if( (i+1)%10000 == 0 ):
        print("Tweets %d of %d has been processed" % ( i+1, nums[1] ))
    clean_tweet_texts.append(tweet_cleaner(df['text'][i]))
len(clean_tweet_texts)

print("Cleaning and parsing the tweets...\n")
for i in range(nums[1],nums[2]):
    if( (i+1)%10000 == 0 ):
        print("Tweets %d of %d has been processed" % ( i+1, nums[2] ))
    clean_tweet_texts.append(tweet_cleaner(df['text'][i]))
len(clean_tweet_texts)

print("Cleaning and parsing the tweets...\n")
for i in range(nums[2],nums[3]):
    if( (i+1)%10000 == 0 ):
        print("Tweets %d of %d has been processed" % ( i+1, nums[3] ))
    clean_tweet_texts.append(tweet_cleaner(df['text'][i]))
len(clean_tweet_texts)

print("Cleaning and parsing the tweets...\n")
for i in range(nums[3],nums[4]):
    if( (i+1)%10000 == 0 ):
        print("Tweets %d of %d has been processed" % ( i+1, nums[4] ))
    clean_tweet_texts.append(tweet_cleaner(df['text'][i]))
len(clean_tweet_texts)

clean_df = pd.DataFrame(clean_tweet_texts,columns=['text'])
clean_df['target'] = df.sentiment
clean_df.head()

clean_df.to_csv('clean_tweet.csv',encoding='utf-8')

csv = 'clean_tweet.csv'
my_df = pd.read_csv(csv,index_col=0)
my_df.head()