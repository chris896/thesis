import torch
from torchtext import data
from torchtext import datasets
import random
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import gensim
from gensim.models.word2vec import Word2Vec
from gensim.models import KeyedVectors

SEED = 2000

torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

TEXT = data.Field(tokenize='spacy')
LABEL = data.LabelField(dtype=torch.float)
ROW_NUM=data.RawField()

csv='clean_tweet2.csv'

my_data = data.TabularDataset(path=csv,
                        format='csv',fields=[('col',ROW_NUM),('text', TEXT), ('lbl', LABEL)])


train_data, valid_data = my_data.split(random_state=random.seed(SEED), split_ratio=0.98)

TEXT.build_vocab(train_data, max_size=100000)
LABEL.build_vocab(train_data)

'''
## pre-trained CBOW & S-G w_v
cbow = 'thesis/Stanford_Sentiment/w2v_model_ug_cbow_M.word2vec'
sg = 'thesis/Stanford_Sentiment/w2v_model_ug_sg_M.word2vec'
model_ug_cbow = KeyedVectors.load(cbow)
model_ug_sg = KeyedVectors.load(sg)
#weights_cbow = torch.FloatTensor(model_ug_cbow.wv)
#weights_sg = torch.FloatTensor(model_ug_sg.vectors)

embeddings_index = {}
for w in model_ug_cbow.wv.vocab.keys():
   embeddings_index[w] = np.append(model_ug_cbow.wv[w], model_ug_sg.wv[w])
'''

BATCH_SIZE = 32

#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = torch.device("cuda:1")

train_iterator, valid_iterator = data.Iterator.splits(
    (train_data, valid_data),
    batch_size=BATCH_SIZE, sort_within_batch=False, sort=False,
    device=device)

print('Number of training examples: ',len(train_data))
print('Number of validation examples: ',len(valid_data))
print("Unique tokens in TEXT vocabulary: ",len(TEXT.vocab))
print("Unique tokens in LABEL vocabulary: ",len(LABEL.vocab))
print(TEXT.vocab.freqs.most_common(20))
print(TEXT.vocab.itos[:10])
print(LABEL.vocab.stoi)

class CNN(nn.Module):
    def __init__(self, vocab_size, embedding_dim, n_filters, filter_sizes, output_dim, dropout):
        super().__init__()

        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.convs = nn.ModuleList(
            [nn.Conv2d(in_channels=1, out_channels=n_filters, kernel_size=(fs, embedding_dim)) for fs in filter_sizes])
        # dense layer
        self.fc = nn.Linear(len(filter_sizes) * n_filters, output_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        # x = [sent len, batch size]

        x = x.permute(1, 0)

        # x = [batch size, sent len]

        embedded = self.embedding(x)

        # embedded = [batch size, sent len, emb dim]

        embedded = embedded.unsqueeze(1)

        # embedded = [batch size, 1, sent len, emb dim]

        conved = [F.relu(conv(embedded)).squeeze(3) for conv in self.convs]

        # conv_n = [batch size, n_filters, sent len - filter_sizes[n]]

        pooled = [F.max_pool1d(conv, conv.shape[2]).squeeze(2) for conv in conved]

        # pooled_n = [batch size, n_filters]

        cat = self.dropout(torch.cat(pooled, dim=1))

        # cat = [batch size, n_filters * len(filter_sizes)]

        return self.fc(cat)


INPUT_DIM = len(TEXT.vocab)
EMBEDDING_DIM = 200
N_FILTERS = 200
FILTER_SIZES = [4,4,4]
OUTPUT_DIM = 1
DROPOUT = 0.0

model = CNN(INPUT_DIM, EMBEDDING_DIM, N_FILTERS, FILTER_SIZES, OUTPUT_DIM, DROPOUT)

#pretrained_embeddings = embeddings_index

#model.embedding.weight.data.copy_(pretrained_embeddings)
#embedding = from_pretrained(weights)

import torch.optim as optim

optimizer = optim.Adam(model.parameters())

criterion = nn.BCEWithLogitsLoss()
#criterion = nn.CrossEntropyLoss()

model = model.to(device)
criterion = criterion.to(device)


def binary_accuracy(preds, y):
    """
    Returns accuracy per batch, i.e. if you get 8/10 right, this returns 0.8, NOT 8
    """

    #round predictions to the closest integer
    rounded_preds = torch.round(torch.sigmoid(preds))
    correct = (rounded_preds == y).float() #convert into float for division
    acc = correct.sum()/len(correct)
    return acc


def sim_target_supervised(target_labels):
    cur_labels = target_labels
    N = cur_labels.shape[0]
    N_labels = 2
    Gt, mask = np.zeros((N, N)), np.zeros((N, N))

    for i in range(N):
        for j in range(N):
            if cur_labels[i] == cur_labels[j]:
                Gt[i, j] = 0.8
                mask[i, j] = 1
            else:
                Gt[i, j] = 0.1
                mask[i, j] = 0.8 / (N_labels - 1)

    return np.float32(Gt), np.float32(mask)


def train(model, iterator, optimizer, criterion):
    epoch_loss = 0
    epoch_acc = 0

    model.train()

    for batch in iterator:
        optimizer.zero_grad()

        # forward - συνδυασμός
        predictions = model(batch.text).squeeze(1)

        loss = criterion(predictions, batch.lbl)

        acc = binary_accuracy(predictions, batch.lbl)

        loss.backward()

        optimizer.step()
        Sim_mat, Mask = sim_target_supervised(batch.lbl)

        epoch_loss += loss.item()
        epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator), Sim_mat, Mask


def evaluate(model, iterator, criterion):
    epoch_loss = 0
    epoch_acc = 0

    model.eval()

    with torch.no_grad():
        for batch in iterator:
            predictions = model(batch.text).squeeze(1)

            loss = criterion(predictions, batch.lbl)

            acc = binary_accuracy(predictions, batch.lbl)

            epoch_loss += loss.item()
            epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator)


N_EPOCHS = 5

for epoch in range(N_EPOCHS):
    train_loss, train_acc, Sim_mat, Mask = train(model, train_iterator, optimizer, criterion)
    valid_loss, valid_acc = evaluate(model, valid_iterator, criterion)
    print("Epoch: {0}".format(epoch+1))

    print("Train_Loss: {0:.3f} | Train_Acc: {1:.2f}% |Val_Loss: {2:.3f} |"
          " Val_Acc: {3:.2f}% ".format(train_loss,train_acc*100,
                                       valid_loss,valid_acc*100))
print(Sim_mat, Mask)
