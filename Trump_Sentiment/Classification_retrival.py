import warnings
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')
import os
from sklearn import utils
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from tqdm import tqdm
tqdm.pandas(desc="progress-bar")
import keras
from keras import backend as K
from keras.models import Sequential
from keras.layers import Dense
from keras.layers.embeddings import Embedding
from keras.layers import Conv1D, GlobalMaxPooling1D
from gensim.models.word2vec import Word2Vec
from gensim.models.doc2vec import TaggedDocument
import multiprocessing
from gensim.models import KeyedVectors
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from time import time
SEED = 2000

# read-set data
csv = 'thesis/Stanford_Sentiment/clean_tweet.csv'
my_df = pd.read_csv(csv,index_col=0)
my_df.dropna(inplace=True)
my_df.reset_index(drop=True,inplace=True)
my_df.info()

x = my_df.text
y = my_df.target

# split train-validation-test data (98%/2%)
x_train, x_val, y_train, y_val = train_test_split\
    (x, y, test_size=.02, random_state=SEED)


print("Train set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_train),
                                                                             (len(x_train[y_train == 0]) / (len(x_train)*1.))*100,
                                                                            (len(x_train[y_train == 1]) / (len(x_train)*1.))*100))
print("Validation set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_val),
                                                                             (len(x_val[y_val == 0]) / (len(x_val)*1.))*100,
                                                                            (len(x_val[y_val == 1]) / (len(x_val)*1.))*100))



input_shape = 45
num_words = 100000
tokenizer = Tokenizer(num_words=num_words)
tokenizer.fit_on_texts(x_train)
sequences = tokenizer.texts_to_sequences(x_train)
#num_words = len(tokenizer.word_index) + 1

dim = 200
x_train_seq = pad_sequences(sequences, maxlen=input_shape)
print('Shape of data tensor:', x_train_seq.shape)

sequences_val = tokenizer.texts_to_sequences(x_val)
x_val_seq = pad_sequences(sequences_val, maxlen=input_shape)


import os
os.environ["CUDA_VISIBLE_DEVICES"]="0"
from keras import backend as K
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
K.set_session(sess)


from keras.models import load_model

# 256 neurons - last layer
loaded_CNN_model = load_model('thesis/Stanford_Sentiment/Best Weights/CNN_best_weights.02-0.8343.hdf5')

# 2 neurons - last layer - tanh activaction dense_2
#loaded_CNN_model = load_model('thesis/Stanford_Sentiment/Best Weights/CNN_19_best_weights.03-0.5022.hdf5')

# sigmoid activation for denses
loaded_CNN_model = load_model('thesis/Stanford_Sentiment/Best Weights/CNN_best_weights.01-0.8105.hdf5')

# relu - sigmoid activation for dense 1-2
## "0" & "1: embeddings
loaded_CNN_model = load_model('thesis/Stanford_Sentiment/Best Weights/CNN_best_weights.01-0.8058.hdf5')




## ~~~~~~~~~~~~~~~~~~~~~~~ Trump retrieval ~~~~~~~~~~~~~~~~~~~~~~~~~ ##

csv2 = 'thesis/Twitter_Trump_Classifier/clean_tweet_Trump_3-L.csv'
trump_df= pd.read_csv(csv2,index_col=0)

X_trump = trump_df.text
y_trump = []

trump_sequences = tokenizer.texts_to_sequences(X_trump)
x_trump_seq = pad_sequences(trump_sequences, maxlen=input_shape)
print('Shape of data tensor:', x_trump_seq.shape)

for x in range(len(x_trump_seq)):
    result = loaded_CNN_model.predict(x_trump_seq[x].reshape(1, x_trump_seq.shape[1]), verbose=2)
    pred = [1 if t > 0.5 else 0 for t in result]
    y_trump.append(pred)


trump_df['Sentiment_Stanford'] = y_trump
trump_df['Sentiment_Stanford'].value_counts()
trump_df.to_csv('thesis/Twitter_Trump_Classifier/TextBlob-NLTK-Stanford Sentiment/clean_labeled_tweet_Trump_Stanford.csv',encoding='utf-8')

import matplotlib.pyplot as plt
num_bins = 21
n, bins, patches = plt.hist(trump_df['Sentiment_Stanford'], num_bins, normed=1, facecolor='green', alpha=0.5)
plt.xlabel('Sentiment')
plt.ylabel('Probability')
plt.title(r'Histogram of sentiment')

plt.subplots_adjust(left=0.15)
plt.show()

