import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import RidgeClassifier
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.linear_model import Perceptron
from sklearn.neighbors import NearestCentroid
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report, confusion_matrix
from time import time
plt.style.use('fivethirtyeight')
seed = 2000

csv = 'thesis/Twitter_Trump_Classifier/clean_tweet_Trump_3-L.csv'
my_df= pd.read_csv(csv,index_col=0)

X = my_df['text']
y = my_df['Sentiment']

# split train-validation-test data (80%/10%/10%)
x_train, x_val, y_train, y_val = train_test_split\
    (X, y, test_size=.2, random_state=seed)

print("Train set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive, {3:.2f}% neutral".format(len(x_train),
                                                                             (len(x_train[y_train == 0]) / (len(x_train)*1.))*100,
                                                                            (len(x_train[y_train == 1]) / (len(x_train)*1.))*100,
                                                                            (len(x_train[y_train == 2]) / (len(x_train)*1.))*100))
print("Validation set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive, {3:.2f}% neutral".format(len(x_val),
                                                                             (len(x_val[y_val == 0]) / (len(x_val)*1.))*100,
                                                                            (len(x_val[y_val == 1]) / (len(x_val)*1.))*100,
                                                                            (len(x_val[y_val == 2]) / (len(x_val)*1.))*100))


##  ~~~~~~~~~~~~~~~~~~~~~~~~~~~ Accuracy summary ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##
def accuracy_summary(pipeline, x_train, y_train, x_test, y_test):
    t0 = time()
    sentiment_fit = pipeline.fit(x_train, y_train)
    y_pred = sentiment_fit.predict(x_test).astype(int)
    train_test_time = time() - t0
    accuracy = accuracy_score(y_test, y_pred)
    print("accuracy score: {0:.2f}%".format(accuracy * 100))
    print("train and test time: {0:.2f}s".format(train_test_time))
    print("-" * 80)
    return accuracy, train_test_time, y_pred

cvec = CountVectorizer()
cvec.fit_transform(x_train)
len(cvec.get_feature_names()) # extracted words out of the corpus 45242

lr = LogisticRegression() # classifier
n_features = np.arange(5000,25001,10000)

# check the accuracy on validation set for the different number of features
def nfeature_accuracy_checker(vectorizer=cvec, n_features=n_features, stop_words=None, ngram_range=(1, 1), classifier=lr):
    result = []
    print (classifier)
    print("\n")
    for n in n_features:
        vectorizer.set_params(stop_words=stop_words, max_features=n, ngram_range=ngram_range)
        checker_pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
        print("Validation result for {} features".format(n))
        nfeature_accuracy,tt_time, pred = accuracy_summary(checker_pipeline, x_train, y_train, x_val, y_val.values)
        result.append((n,nfeature_accuracy,tt_time))
    return result, pred


##time - Results about stop words with count-vectorizer
#print("RESULT FOR UNIGRAM WITHOUT STOP WORDS\n")
#feature_result_wosw = nfeature_accuracy_checker(stop_words='english')

print("RESULT FOR UNIGRAM WITH STOP WORDS\n")
feature_result_ug,pred = nfeature_accuracy_checker()

# Bigram with stop words
print("RESULT FOR BIGRAM WITH STOP WORDS\n")
feature_result_bg = nfeature_accuracy_checker(ngram_range=(1, 2))

# Trigram
print("RESULT FOR TRIGRAM WITH STOP WORDS\n")
feature_result_tg = nfeature_accuracy_checker(ngram_range=(1, 3))

# check accuracy & confussion matrix for max num of features
def train_test_and_evaluate(pipeline, x_train, y_train, x_test, y_test):
    sentiment_fit = pipeline.fit(x_train, y_train)
    y_pred = sentiment_fit.predict(x_test).astype(int)
    accuracy = accuracy_score(y_test, y_pred)
    conmat = np.array(confusion_matrix(y_test, y_pred, labels=[0,1,2]))
    confusion = pd.DataFrame(conmat, index=['negative','positive','neutral'],
                columns=['predicted_negative','predicted_positive', 'predicted_neutral'])
    print("accuracy score: {0:.2f}%".format(accuracy*100))
    print("-"*80)
    print("Confusion Matrix\n")
    print(confusion)
    print("-"*80)
    print("Classification Report\n")
    print(classification_report(y_test, y_pred, target_names=['negative','positive','neutral']))
    print('\n')


# best performing number of features with each n-gram
# unigrams - 25.000 features
ug_cvec = CountVectorizer(max_features=25000)
ug_pipeline = Pipeline([
        ('vectorizer', ug_cvec),
        ('classifier', lr)
    ])
train_test_and_evaluate(ug_pipeline, x_train, y_train, x_val, y_val)

# bigrams - 30000 features
bg_cvec = CountVectorizer(max_features=30000,ngram_range=(1, 2))
bg_pipeline = Pipeline([
        ('vectorizer', bg_cvec),
        ('classifier', lr)
    ])
train_test_and_evaluate(bg_pipeline, x_train, y_train, x_val, y_val)

# trigram - 5.000 features
tg_cvec = CountVectorizer(max_features=5.000, ngram_range=(1, 3))
tg_pipeline = Pipeline([
        ('vectorizer', tg_cvec),
        ('classifier', lr)
    ])
train_test_and_evaluate(tg_pipeline, x_train, y_train, x_val, y_val)

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TFIDF Vectorizer ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##

from sklearn.feature_extraction.text import TfidfVectorizer
tvec = TfidfVectorizer()
tvec.fit_transform(x_train)
len(tvec.get_feature_names()) # extracted words out of the corpus 6526

print("RESULT FOR UNIGRAM WITH STOP WORDS (Tfidf)\n")
feature_result_ugt = nfeature_accuracy_checker(vectorizer=tvec,ngram_range=(1,1))

print("RESULT FOR BIGRAM WITH STOP WORDS (Tfidf)\n")
feature_result_bgt = nfeature_accuracy_checker(vectorizer=tvec,ngram_range=(1, 2))

print("RESULT FOR TRIGRAM WITH STOP WORDS (Tfidf)\n")
feature_result_tgt = nfeature_accuracy_checker(vectorizer=tvec,ngram_range=(1, 3))

# best performing number of features with each n-gram
# unigrams - 45.000 features
ug_cvec = TfidfVectorizer(max_features=45000)
ug_pipeline = Pipeline([
        ('vectorizer', ug_cvec),
        ('classifier', lr)
    ])
train_test_and_evaluate(ug_pipeline, x_train, y_train, x_val, y_val)

# bigrams - 30.000 features
bg_cvec = TfidfVectorizer(max_features=30000,ngram_range=(1, 2))
bg_pipeline = Pipeline([
        ('vectorizer', bg_cvec),
        ('classifier', lr)
    ])
train_test_and_evaluate(bg_pipeline, x_train, y_train, x_val, y_val)

# trigram - 600 features
tg_cvec = TfidfVectorizer(max_features=1000,ngram_range=(1, 3))
tg_pipeline = Pipeline([
        ('vectorizer', tg_cvec),
        ('classifier', lr)
    ])
train_test_and_evaluate(tg_pipeline, x_train, y_train, x_val, y_val)


## ~~~~~~~~~~~~~~~~~~~~~~~~ Classifier comparator & Voting ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##

names = ["Logistic Regression", "Linear SVC", "LinearSVC with L1-based feature selection","Multinomial NB",
         "Bernoulli NB", "Ridge Classifier", "AdaBoost", "Perceptron","Passive-Aggresive", "Nearest Centroid"]
classifiers = [
    LogisticRegression(),
    LinearSVC(),
    Pipeline([
  ('feature_selection', SelectFromModel(LinearSVC(penalty="l1", dual=False))),
  ('classification', LinearSVC(penalty="l2"))]),
    MultinomialNB(),
    BernoulliNB(),
    RidgeClassifier(),
    AdaBoostClassifier(),
    Perceptron(),
    PassiveAggressiveClassifier(),
    NearestCentroid()
    ]
zipped_clf = zip(names,classifiers)

def classifier_comparator(vectorizer=cvec, n_features=25000, stop_words=None, ngram_range=(1, 1), classifier=zipped_clf):
    result = []
    vectorizer.set_params(stop_words=stop_words, max_features=n_features, ngram_range=ngram_range)
    for n,c in classifier:
        checker_pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', c)
        ])
        print("Validation result for {}".format(n))
        print(c)
        clf_accuracy,tt_time = accuracy_summary(checker_pipeline, x_train, y_train, x_val, y_val)
        result.append((n,clf_accuracy,tt_time))
    return result

unigram_result = classifier_comparator(n_features=25000, ngram_range=(1,1))


from sklearn.ensemble import VotingClassifier

clf1 = LogisticRegression()
clf2 = LinearSVC(penalty="l2")
clf3 = Perceptron()
clf4 = RidgeClassifier()
clf5 = PassiveAggressiveClassifier()

eclf = VotingClassifier(estimators=[('lr', clf1), ('svc', clf2), ('prc', clf3), ('rcs', clf4), ('pac', clf5)], voting='hard')

for clf, label in zip([clf1, clf2, clf3, clf4, clf5, eclf], ['Logistic Regression', 'Linear SVC', 'Perceptron', 'Ridge Classifier', 'Passive Aggresive Classifier', 'Ensemble']):
    checker_pipeline = Pipeline([
            ('vectorizer', CountVectorizer(max_features=25000,ngram_range=(1, 1))),
            ('classifier', clf)
        ])
    print("Validation result for {}".format(label))
    print(clf)
    clf_accuracy,tt_time = accuracy_summary(checker_pipeline, x_train, y_train, x_val, y_val)

