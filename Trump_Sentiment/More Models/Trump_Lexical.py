'''
In the lexical approach the definition of sentiment is based on the analysis of individual words and/or phrases;
emotional dictionaries are often used: emotional lexical items from the dictionary are searched in the text,
their sentiment weights are calculated, and some aggregated weight function is applied.
'''

from time import time
import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer

'''
positive rate CDF & positive frequency percent CDF give me a good representation of
positive and negative terms in the corpus. If it successfully filters which terms are important
to each class, then this can also be used for prediction in lexical manner
 '''

csv= 'clean_tweet_Trump_class.csv'
my_df = pd.read_csv(csv)


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~ Running the Model ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
X = np.array(my_df['text'])
y = np.array(my_df['Sentiment'])


SEED = 2000

X_train, X_vt, y_train, y_vt = train_test_split(X, y, random_state=SEED, test_size=0.2)

X_val, X_test, y_val, y_test = train_test_split(X_vt, y_vt, random_state=SEED, test_size=0.5)

print("Train set has total {0} entries with {1:.2f}% hate, {2:.2f}% offensive".format(len(X_train),(len(X_train[y_train == 0]) / (len(X_train)*1.))*100,
                                                                            (len(X_train[y_train == 1]) / (len(X_train)*1.))*100))

print("Validation set has total {0} entries with {1:.2f}% hate, {2:.2f}% offensive".format(len(X_val),(len(X_val[y_val == 0]) / (len(X_val)*1.))*100,
                                                                            (len(X_val[y_val == 1]) / (len(X_val)*1.))*100,))

print("Test set has total {0} entries with {1:.2f}% hate, {2:.2f}% offensive".format(len(X_test),(len(X_test[y_test == 0]) / (len(X_test)*1.))*100,
                                                                            (len(X_test[y_test == 1]) / (len(X_test)*1.))*100))

t0 = time()
cvec = CountVectorizer(max_features=2000)
cvec.fit(X_train)

'''
term frequency calculation to get pos_normcdf_hmean,
calculate term frequency only from the train set
pos_normcdf_hmean metric provides a more meaningful measure of how important a word is within the class
'''

neg_train = X_train[y_train == 0]
pos_train = X_train[y_train == 1]

neg_doc_matrix = cvec.transform(neg_train)
pos_doc_matrix = cvec.transform(pos_train)
#neither_doc_matrix = cvec.transform((neither_train))


neg_tf = np.sum(neg_doc_matrix,axis=0)
pos_tf = np.sum(pos_doc_matrix,axis=0)
#neither_tf = np.sum(neither_doc_matrix, axis=0)

from scipy.stats import hmean
from scipy.stats import norm
def normcdf(x):
    return norm.cdf(x, x.mean(), x.std())

neg = np.squeeze(np.asarray(neg_tf))
pos = np.squeeze(np.asarray(pos_tf))

term_freq_df2 = pd.DataFrame([neg,pos],columns=cvec.get_feature_names()).transpose()
term_freq_df2.columns = ['neg','pos']
term_freq_df2['total'] = term_freq_df2['neg'] + term_freq_df2['pos']
term_freq_df2['pos_rate'] = term_freq_df2['pos'] * 1./term_freq_df2['total']
term_freq_df2['pos_freq_pct'] = term_freq_df2['pos'] * 1./term_freq_df2['pos'].sum()
term_freq_df2['pos_rate_normcdf'] = normcdf(term_freq_df2['pos_rate'])
term_freq_df2['pos_freq_pct_normcdf'] = normcdf(term_freq_df2['pos_freq_pct'])
term_freq_df2['pos_normcdf_hmean'] = hmean([term_freq_df2['pos_rate_normcdf'], term_freq_df2['pos_freq_pct_normcdf']])
term_freq_df2.sort_values(by='pos_normcdf_hmean', ascending=False).iloc[:10]


'''
For each word in a document, look it up in the list of 10,000 words I built vocabulary with, and get the corresponding
pos_normcdf_hmean value, then for the document calculate the average pos_normcdf_hmean value.
If none of the words can be found from the built 10,000 terms, then yields random probability ranging between 0 to 1
'''

pos_hmean = term_freq_df2.pos_normcdf_hmean
# hate_hmean['wtf']

y_val_predicted_proba = []
for t in X_val:
    hmean_scores = [pos_hmean[w] for w in t.split() if w in pos_hmean.index] # for each sentence find hmean for each word
    if len(hmean_scores) > 0:
        prob_score = np.mean(hmean_scores) # mean of hmean for the each sentence
    else:
        prob_score = np.random.random() # give random hmean
    y_val_predicted_proba.append(prob_score)

# threshold for hate = 0.6 (from the average of pos) for best result t > 60%
pred = [1 if t > 0.6 else 0 for t in y_val_predicted_proba]

from sklearn.metrics import accuracy_score
val_acc = accuracy_score(y_val,pred) # 92.26 %

# Measuring precision, recall and F1-score for every class
pos_correct, neg_correct = 0, 0
neg_pred, pos_pred = 0, 0
pos_cnt, neg_cnt  = 0, 0

for x in range(len(X_val)):
    if pred[x] == 0:
        neg_pred += 1
    else:
        pos_pred += 1

    if pred[x] == y_val[x]:
        if y_val[x] == 0:
            neg_correct += 1
        else :
            pos_correct += 1

    if (y_val[x]) == 0:
        neg_cnt += 1
    else:
        pos_cnt += 1


print("Best validation accuracy: %.2f%%"% (val_acc*100))


print ("\n")
print("%.2f%%"% ((float(neg_correct) / float(neg_pred))*100)+ " neg precision")
print("%.2f%%"% ((float(pos_correct) / float(pos_pred))*100)+ " pos precision")


print ("\n")
print("%.2f%%"% ((float(neg_correct) / float(neg_cnt))*100)+ " neg recall")
print("%.2f%%"% ((float(pos_correct) / float(pos_cnt))*100)+ " pos recall")


print ("\n")
print("%.2f%%"% float(2*((((float(neg_correct) / float(neg_pred))*100)*((float(neg_correct) / float(neg_cnt))*100))/(((float(neg_correct) / float(neg_pred))*100)+((float(neg_correct) / float(neg_cnt))*100))))
      + " hate F1_score")
print("%.2f%%"% float(2*((((float(pos_correct) / float(pos_pred))*100)*((float(pos_correct) / float(pos_cnt))*100))/(((float(pos_correct) / float(pos_pred))*100)+((float(pos_correct) / float(pos_cnt))*100))))
      + " offensive F1_score")

train_test_time = time() - t0
print ("\n")
print("Train & test time: {} sec".format(train_test_time))
print ("-" * 80)
