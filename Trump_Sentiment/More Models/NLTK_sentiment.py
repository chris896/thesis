from nltk.classify import NaiveBayesClassifier
from nltk.corpus import subjectivity
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.sentiment.util import *
import pandas as pd

tweets=[]

with open('thesis/Stanford_Sentiment/clean_tweet2.csv', 'r+') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    next(reader)
    for row in reader:

        tweet = dict()
        tweet['text'] = row[1]
        tweet['id'] = int(row[0])
        tweet['Sentiment_Stanford'] = row[2]

        # Create textblob object
        # tweet['TextBlob'] = TextBlob(tweet['orig'])

        # Correct spelling (WARNING: SLOW)
        # tweet['TextBlob'] = tweet['TextBlob'].correct()
        tweets.append(tweet)
    df = pd.DataFrame(tweets, columns=['id', 'text', 'Sentiment_Stanford'])

sentiment = []
for tweet in tweets:
    sid = SentimentIntensityAnalyzer()
    ss = sid.polarity_scores(tweet['text'])
    if ss["compound"] == 0.0:
        tweet['sentiment'] = 'neutral'
        sentiment.append(tweet['sentiment'])
    elif ss["compound"] > 0.0:
        tweet['sentiment'] = 'positive'
        sentiment.append(tweet['sentiment'])
    else:
        tweet['sentiment'] = 'negative'
        sentiment.append(tweet['sentiment'])

df['Sentiment_NLTK'] = sentiment
df['Sentime_Stanford']= df['Sentiment_Stanford'].map({'0':'negative', '1':'positive'})
df.to_csv('thesis/Stanford_Sentiment/labeled_tweet_Stanford_NLTK.csv',encoding='utf-8')

import matplotlib.pyplot as plt
num_bins = 21
n, bins, patches = plt.hist(sentiment, num_bins, normed=1, facecolor='green', alpha=0.5)
plt.xlabel('Sentiment')
plt.ylabel('Probability')
plt.title(r'Histogram of sentiment')
# Tweak spacing to prevent clipping of ylabel
plt.subplots_adjust(left=0.15)
plt.show()

df['Sentiment_NLTK'].value_counts()

'''
positive    777558 (48.69%)
negative    421590 (26.40%)
neutral     397604 (24.90%)
'''