import sys
import numpy as np
import pandas as pd
import csv
import tweepy
import os
import aylienapiclient
import matplotlib.pyplot as plt
from collections import Counter
from aylienapiclient import textapi
from aylienapiclient.errors import HttpError
#import spacy
#from spacy.lang.en import English

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Create dataset ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##

open_kwargs = {}

if sys.version_info[0] < 3:
    input = raw_input
else:
    open_kwargs = {'newline': ''}


if sys.version_info[0] < 3:
   input = raw_input

## Twitter credentials
consumer_key = "D6WH8SvRkd5xM7dEIS0uHZ162"
consumer_secret = "cM07ohRS7oyrdxgJTFiD3zIvBaKvOMfBqXsFSxbfDnCDPRuXiV"
access_token = "984879349886308352-cRhqJ0F1SkdNSiXzEFDEBcS4FgSGtnX"
access_token_secret = "hz7aKPeCBflQFtEOdX1gctZpTq6aSvtbO3TKkZUHs11gJ"

## AYLIEN credentials
application_id = "f3e7611f"
application_key = "acf9428509c53fd843eae7efd704ec80"

## set up an instance of Tweepy
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

## set up an instance of the AYLIEN Text API
client = textapi.Client(application_id, application_key)

## search Twitter for something that interests you
query = "Trump"
number = 60
#query = input("What subject do you want to analyze for this example? \n")
#number = input("How many Tweets do you want to analyze? \n")

#here insert a few lines to open the previous CSV file and read the last entry for id
max_id = 0

## open a csv file to store the Tweets and their sentiment
file_name = 'Sentiment_Analysis_of_Tweets_About_{}2.csv'.format(query)

if os.path.exists(file_name):
    with open(file_name, 'r') as f:
        for row in csv.DictReader(f):
            max_id = row['Tweet_ID']
else:
	    with open(file_name, 'w', **open_kwargs) as f:
	        csv.writer(f).writerow([
	                                "Tweet_ID",
	                                "Time",
	                                "Tweet",
	                                "Sentiment",
                                    "Confidence"])

results = api.search(
   lang="en",
   q=query + " -rt", ## exclude retweets
   count=number,  ## limit = 100
   result_type="mixed",  ## run the most recent tweets (current/popular/mixed)
   since_id=max_id
)

results = sorted(results, key=lambda x: x.id)

print("--- Gathered Tweets \n")

# open a csv file to store the Tweets and their sentiment
with open(file_name, 'a', **open_kwargs) as csvfile:
    csv_writer = csv.DictWriter(
        f=csvfile,
        fieldnames=[
                    "Tweet_ID",
                    "Time",
                    "Tweet",
                    "Sentiment",
                    "Confidence"]
    )

    print("--- Opened a CSV file to store the results of your sentiment analysis... \n")

    # tidy up the Tweets and send each to the AYLIEN Text API
    for c, result in enumerate(results, start=1):
        tweet = result.text

        tidy_tweet = tweet.strip().encode('ascii', 'ignore')
        tweet_time = result.created_at
        tweet_id = result.id

        if not tweet:
            print('Empty Tweet')
            continue

        response = client.Sentiment({'text': tidy_tweet})
        csv_writer.writerow({
        	"Tweet_ID": tweet_id,
        	"Time": tweet_time,
            'Tweet': response['text'],
            'Sentiment': response['polarity'],
            'Confidence': response['polarity_confidence']
        })

        print("Analyzed Tweet {}".format(c))

## count the data in the Sentiment column of the CSV file
with open(file_name, 'r') as data:
   counter = Counter()
   for row in csv.DictReader(data):
       counter[row['Sentiment']] += 1

   positive = counter['positive']
   negative = counter['negative']
   neutral = counter['neutral']

## declare the variables for the pie chart, using the Counter variables for "sizes"
colors = ['green', 'red', 'grey']
sizes = [positive, negative, neutral]
labels = 'Positive', 'Negative', 'Neutral'

## use matplotlib to plot the chart
plt.pie(
   x=sizes,
   shadow=True,
   colors=colors,
   labels=labels,
   startangle=90
)

plt.title("Sentiment of {} Tweets about {}2".format(sum(counter.values()), query))
#plt.show()


## ~~~~~~~~~~~~~~~~~~~~~~~~~~ 2. Data preparation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# read csv

df = pd.read_csv("Sentiment_Analysis_of_Tweets_About_Trump2.csv")
# devide text - polarity - confidence
tweets=df['Tweet']
sentiment=df['Sentiment']
confidence=df['Confidence']

df['Sentiment'].value_counts()
# df.describe()
# columns: Tweet_ID,Time,Tweet,Sentiment,Confidence
df.columns
df.drop(['Tweet_ID','Time'],axis=1,inplace=True)
#df.drop(df.columns[Null], axis=1)

df['Sentiment'].hist()
#plt.show()

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 3. Feature generation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from bs4 import BeautifulSoup
import re
from nltk.tokenize import WordPunctTokenizer
tok = WordPunctTokenizer()

pat1 = r'@[A-Za-z0-9]+'
pat2 = r'https?://[A-Za-z0-9./]+'
combined_pat = r'|'.join((pat1, pat2))

def tweet_cleaner(text):
    soup = BeautifulSoup(text, 'html.parser')
    souped = soup.get_text()
    stripped = re.sub(combined_pat, '', souped)
    try:
        clean = stripped.decode("utf-8-sig").replace(u"\ufffd", "?")
    except:
        clean = stripped
    letters_only = re.sub("[^a-zA-Z]", " ", clean)
    lower_case = letters_only.lower()
    # During the letters_only process two lines above, it has created unnecessay white spaces,
    # I will tokenize and join together to remove unneccessary white spaces
    words = tok.tokenize(lower_case)
    return (" ".join(words)).strip()

clean_tweet_texts_Trump = []
for t in tweets:
    clean_tweet_texts_Trump.append(tweet_cleaner(t))

clean_df = pd.DataFrame(clean_tweet_texts_Trump,columns=['text'])
clean_df['Sentiment'] = sentiment
clean_df.to_csv('clean_tweet_Trump2.csv',encoding='utf-8')

# read csv
csv = 'clean_tweet_Trump2.csv'
my_df = pd.read_csv(csv,index_col=0)
my_df.head()

# find null entries and drop them
my_df[my_df.isnull().any(axis=1)].head()
np.sum(my_df.isnull().any(axis=1))
my_df.isnull().any(axis=0)
my_df.dropna(inplace=True)
my_df.info()

my_df.to_csv('clean_tweet_Trump2.csv',encoding='utf-8')

# txt
text = []
text.extend(my_df['text'])
doc = open("clean_tweet_Trump2.txt", 'w')
for i in range(0,len(text)):
    doc.writelines(text[i] + '. ')
    #print(text[i] + '. ')
doc.close()

#text = open('clean_tweet_Trump.txt', 'r').read() # open a document

SEED = 2000

plt.style.use('fivethirtyeight')

my_df['Sentiment'] = my_df['Sentiment'].map({"negative": 0, "neutral":2, "positive": 1})
tweets = my_df['text']
sent = my_df['Sentiment']

my_df.Sentiment
df_filter = my_df[my_df.Sentiment==2]
df = my_df.drop(index=df_filter.index)

# read csv for classification
df.to_csv('clean_tweet_Trump_class2.csv',encoding='utf-8')
csv = 'clean_tweet_Trump_class2.csv'
my_df_class= pd.read_csv(csv,index_col=0)

X = np.array(my_df_class['text'])
y = np.array(my_df_class['Sentiment'])

print(my_df_class['Sentiment'].value_counts())