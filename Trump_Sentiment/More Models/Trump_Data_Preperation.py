import numpy as np
import pandas as pd
import csv
import matplotlib.pyplot as plt
from collections import Counter

## search Twitter for something that interests you
query = "Trump"

## open a csv file to store the Tweets and their sentiment
file_name = 'Twitter_Trump_Classifier\Sentiment_Analysis_of_Tweets_About_{}.csv'.format(query)

## count the data in the Sentiment column of the CSV file
with open(file_name, 'r') as data:
   counter = Counter()
   for row in csv.DictReader(data):
       counter[row['Sentiment']] += 1

   positive = counter['positive']
   negative = counter['negative']
   neutral = counter['neutral']

## declare the variables for the pie chart, using the Counter variables for "sizes"
colors = ['green', 'red', 'grey']
sizes = [positive, negative, neutral]
labels = 'Positive', 'Negative', 'Neutral'

## use matplotlib to plot the chart
plt.pie(
   x=sizes,
   shadow=True,
   colors=colors,
   labels=labels,
   startangle=90
)

plt.title("Sentiment of {} Tweets about {}".format(sum(counter.values()), query))
plt.show()


## ~~~~~~~~~~~~~~~~~~~~~~~~~~ 2. Data preparation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# read csv
df = pd.read_csv("thesis/Twitter_Trump_Classifier/Sentiment_Analysis_of_Tweets_About_Trump.csv")
# devide text - polarity - confidence
tweets=df['Tweet']
sentiment=df['Sentiment']
confidence=df['Confidence']

df['Sentiment'].value_counts()
# df.describe()
# columns: Tweet_ID,Time,Tweet,Sentiment,Confidence
df.columns
df.drop(['Tweet_ID','Time','Confidence'],axis=1,inplace=True)
#df.drop(df.columns[Null], axis=1)

df['Sentiment'].hist()
plt.show()

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 3. Feature generation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
from bs4 import BeautifulSoup
import re
from nltk.tokenize import WordPunctTokenizer
tok = WordPunctTokenizer()

pat1 = r'@[A-Za-z0-9]+' # remove "@"
pat2 = r'https?://[A-Za-z0-9./]+' # remove url links
combined_pat = r'|'.join((pat1, pat2))

def tweet_cleaner(text):
    soup = BeautifulSoup(text, 'html.parser') # html decoding ("@amp")
    souped = soup.get_text()
    stripped = re.sub(combined_pat, '', souped)
    try:
        clean = stripped.decode("utf-8-sig").replace(u"\ufffd", "?")  # remove hashtag/numbers
    except:
        clean = stripped
    letters_only = re.sub("[^a-zA-Z]", " ", clean)
    lower_case = letters_only.lower()
    # During the letters_only process two lines above, it has created unnecessay white spaces,
    # I will tokenize and join together to remove unneccessary white spaces
    words = tok.tokenize(lower_case)
    return (" ".join(words)).strip()

clean_tweet_texts_Trump = []
for t in tweets:
    clean_tweet_texts_Trump.append(tweet_cleaner(t))

clean_df = pd.DataFrame(clean_tweet_texts_Trump,columns=['text'])
clean_df['Sentiment'] = sentiment
clean_df.to_csv('Twitter_Trump_Classifier\clean_tweet_Trump.csv',encoding='utf-8')

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##
# read csv
csv = 'thesis\Twitter_Trump_Classifier\clean_tweet_Trump.csv'
my_df = pd.read_csv(csv,index_col=0)
my_df.head()

# find null entries and drop them
my_df[my_df.isnull().any(axis=1)].head()
np.sum(my_df.isnull().any(axis=1))
my_df.isnull().any(axis=0)
my_df.dropna(inplace=True)
my_df.info()

my_df.to_csv('thesis\Twitter_Trump_Classifier\clean_tweet_Trump.csv',encoding='utf-8')
my_df['Sentiment'] = my_df['Sentiment'].map({"negative": 0, "neutral":2, "positive": 1})
text = my_df['text']
sentiment = my_df['Sentiment']

df_filter = my_df[my_df.Sentiment==2]
df = my_df.drop(index=df_filter.index)

# read csv where i saved pos & neg data, for classification
df.to_csv('Twitter_Trump_Classifier\clean_tweet_Trump_2-L.csv',encoding='utf-8')
csv = 'Twitter_Trump_Classifier\clean_tweet_Trump_binary.csv'
my_df_class= pd.read_csv(csv,index_col=0)



## ~~~~~~~~~~~~~~~~ Aylien-Stan-NLTK-TextBlob MATCHES ~~~~~~~~~~~~~~~~~ ##
import pandas as pd
import numpy as np
csv = 'thesis/Trump_Sentiment/TextBlob-NLTK-Stanford Sentiment/more/clean_labeled_tweet_Trump_NLTK-TextBlob.csv'
my_df = pd.read_csv(csv)
my_df['Sentiment_Aylien'] = my_df['Sentiment_Aylien'].map({0: 'negative', 1: 'positive', 2: 'neutral'})
matches=[]
k3, k2 = 0,0

for (sa,sn,st) in zip(my_df.Sentiment_Aylien, my_df.Sentiment_NLTK, my_df.Sentiment_TextBlob):
    if (sa == sn) and (sn != st):
        matches.append('Aylien & NLTK MATCH ')
        k2 += 1
    elif (sa == st) and (sa != sn):
        matches.append('Aylien & TextBlob MATCH ')
        k2 += 1
    elif (sn == st) and (sn != sa):
        matches.append('TextBlob & NLTK MATCH ')
        k2 += 1
    elif (sa == sn and sn == st and sa == st) or (sn==st and sa == st) or (sa==sn and sa==st):
        matches.append('ALL MATCH ')
        k3 += 1
    else:
        matches.append('NO MATCH')

my_df['Match/No match'] = matches
agreement_2 = (float(k2) / my_df.shape[0]) *100 # 61.57%
agreement_3 = (float(k3) / my_df.shape[0]) *100 # 28.22%
print("Percentage of agreement for 2 tools is {0:.2f} % & "
      "for all tools is: {1:.2f}%".format(agreement_2,agreement_3))

my_df.to_csv('thesis/Trump_Sentiment/TextBlob-NLTK-Stanford Sentiment/Tools_matches.csv',encoding='utf-8')
my_df['Match/No match'].value_counts()

'''

ALL MATCH                   33828 (28.22%)
Aylien & TextBlob MATCH     29215 (24.37%)
TextBlob & NLTK MATCH       25981 (21.67%)
Aylien & NLTK MATCH         18594 (15.51%)
NO MATCH                    12237 (10.20%)

'''