
# The main package to help us with our text analysis
from textblob import TextBlob
import csv
import re
import operator
import numpy as np
import pandas as pd
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

# Intialize an empty list to hold all of our tweets
tweets = []

# A helper function that removes all the non ASCII characters
# from the given string. Retuns a string with only ASCII characters.
def strip_non_ascii(string):
    ''' Returns the string without non ASCII characters'''
    stripped = (c for c in string if 0 < ord(c) < 127)
    return ''.join(stripped)


with open('thesis/Stanford_Sentiment/clean_tweet2.csv', 'r+') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    next(reader)
    for row in reader:

        tweet= dict()
        tweet['text'] = row[1]
        tweet['id'] = int(row[0])
        tweet['Sentiment_Stanford'] = row[2]

        # Create textblob object
        #tweet['TextBlob'] = TextBlob(tweet['orig'])

        # Correct spelling (WARNING: SLOW)
        #tweet['TextBlob'] = tweet['TextBlob'].correct()
        tweets.append(tweet)
    df = pd.DataFrame(tweets, columns=['id', 'text','Sentiment_Stanford'])

# DEVELOP MODELS
sentiment = []
for tweet in tweets:
    tweet['polarity'] = float(TextBlob(tweet['text']).sentiment.polarity)

    if tweet['polarity'] >= 0.1:
        tweet['sentiment'] = 'positive'
        sentiment.append(tweet['sentiment'])
    elif tweet['polarity'] <= -0.1:
        tweet['sentiment'] = 'negative'
        sentiment.append(tweet['sentiment'])
    else:
        tweet['sentiment'] = 'neutral'
        sentiment.append(tweet['sentiment'])

df['Sentiment_TextBlob'] = sentiment
df['Sentiment_Stanford']= df['Sentiment_Stanford'].map({'0':'negative', '1':'positive'})
df.to_csv('thesis/Stanford_Sentiment/labeled_tweet_Stanford_TextBlob.csv',encoding='utf-8')

# EVALUATE RESULTS

num_bins = 21
n, bins, patches = plt.hist(sentiment, num_bins, normed=1, facecolor='green', alpha=0.5)
plt.xlabel('Sentiment')
plt.ylabel('Probability')
plt.title(r'Histogram of sentiment')
# Tweak spacing to prevent clipping of ylabel
plt.subplots_adjust(left=0.15)
plt.show()

df['Sentiment_TextBlob'].value_counts()

'''
neutral     671928 (42.08%)
positive    643929 (40.32%)
negative    280895 (17.59%)

'''