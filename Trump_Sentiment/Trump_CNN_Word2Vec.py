import warnings
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')
import os
from sklearn import utils
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from tqdm import tqdm
tqdm.pandas(desc="progress-bar")
import keras
from keras import backend as K
from keras.models import Sequential
from keras.layers import Dense
from keras.layers.embeddings import Embedding
from keras.layers import Conv1D, GlobalMaxPooling1D
from gensim.models.word2vec import Word2Vec
from gensim.models.doc2vec import TaggedDocument
import multiprocessing
from gensim.models import KeyedVectors
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from time import time
seed = 2000

csv = 'clean_tweet_Trump_3-L.csv'
my_df= pd.read_csv(csv,index_col=0)

X = my_df.text
y = my_df.Sentiment

# split train-validation-test data (80%/20%)
x_train, x_val, y_train_init, y_val_init = train_test_split\
    (X, y, test_size=.2, random_state=seed)

print("Train set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive, {3:.2f}% neutral".format(len(x_train),
                                                                             (len(x_train[y_train_init == 0]) / (len(x_train)*1.))*100,
                                                                            (len(x_train[y_train_init == 1]) / (len(x_train)*1.))*100,
                                                                            (len(x_train[y_train_init == 2]) / (len(x_train)*1.))*100))
print("Validation set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive, {3:.2f}% neutral".format(len(x_val),
                                                                             (len(x_val[y_val_init == 0]) / (len(x_val)*1.))*100,
                                                                            (len(x_val[y_val_init == 1]) / (len(x_val)*1.))*100,
                                                                            (len(x_val[y_val_init == 2]) / (len(x_val)*1.))*100))


y_train = keras.utils.to_categorical(y_train_init,3)
y_val = keras.utils.to_categorical(y_val_init,3)

'''
                ## Gensim's models training ##

# Labelise each tweet with unique IDs using Gensim’s LabeledSentence function
def labelize_tweets_ug(tweets,label):
    result = []
    prefix = label
    for i, t in zip(tweets.index, tweets):
        result.append(TaggedDocument(t.split(), [prefix + '_%s' % i]))
    return result

all_x = pd.concat([x_train,x_val])
all_x_w2v = labelize_tweets_ug(all_x, 'all')

#CBOW
cores = multiprocessing.cpu_count()
model_ug_cbow = Word2Vec(sg=0, size=100, negative=5, window=2, min_count=2, workers=cores, alpha=0.065, min_alpha=0.065)
model_ug_cbow.build_vocab([x.words for x in tqdm(all_x_w2v)])

st_time = time()
# train Word2Vec model
for epoch in range(30):
    model_ug_cbow.train(utils.shuffle([x.words for x in tqdm(all_x_w2v)]), total_examples=len(all_x_w2v), epochs=1)
    model_ug_cbow.alpha -= 0.002
    model_ug_cbow.min_alpha = model_ug_cbow.alpha

# SKIP-GRAM
model_ug_sg = Word2Vec(sg=1, size=100, negative=5, window=2, min_count=2, workers=cores, alpha=0.065, min_alpha=0.065)
model_ug_sg.build_vocab([x.words for x in tqdm(all_x_w2v)])

for epoch in range(30):
    model_ug_sg.train(utils.shuffle([x.words for x in tqdm(all_x_w2v)]), total_examples=len(all_x_w2v), epochs=1)
    model_ug_sg.alpha -= 0.002
    model_ug_sg.min_alpha = model_ug_sg.alpha

w2v_train_time = time() - st_time
print(w2v_train_time, 'sec')

# save models to .word2vec files
model_ug_cbow.save('thesis/Twitter_Trump_Classifier/w2v_model_ug_cbow_Trump_3-L.word2vec')
model_ug_sg.save('thesis/Twitter_Trump_Classifier/w2v_model_ug_sg_Trump_3-L.word2vec')

'''

model_ug_cbow = KeyedVectors.load('w2v_model_ug_cbow_Trump_3-L.word2vec')
model_ug_sg = KeyedVectors.load('w2v_model_ug_sg_Trump_3-L.word2vec')
#model_g = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True, encoding='utf-8')
#model_w = KeyedVectors.load_word2vec_format('wiki-news-300d-1M.vec', binary=False, encoding='utf-8')
len(model_ug_cbow.wv.vocab.keys())

embeddings_index = {}
for w in model_ug_cbow.wv.vocab.keys():
   embeddings_index[w] = np.append(model_ug_cbow.wv[w], model_ug_sg.wv[w])  # concat two models
#for w in model_w.wv.vocab.keys():
#   embeddings_index[w] = model_w.wv[w]
print('Found %s word vectors.' % len(embeddings_index))  # word vectors

# num_words = the maximum number of words to keep, based on word frequency.
# Only the most common words will be kept.
input_shape = 40
tokenizer = Tokenizer()
tokenizer.fit_on_texts(x_train)
sequences = tokenizer.texts_to_sequences(x_train)
num_words = len(tokenizer.word_index) + 1

dim = 200
x_train_seq = pad_sequences(sequences, maxlen=input_shape)
print('Shape of data tensor:', x_train_seq.shape)

sequences_val = tokenizer.texts_to_sequences(x_val)
x_val_seq = pad_sequences(sequences_val, maxlen=input_shape)


embedding_matrix = np.zeros((num_words, dim))
for word, i in tokenizer.word_index.items():
    if i >= num_words:
        continue
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector


import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"
from keras import backend as K
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
K.set_session(sess)

np.random.seed(seed)
model_cnn_03 = Sequential()
e = Embedding(num_words, dim, weights=[embedding_matrix], input_length=40, trainable=True)
#e = Embedding(num_words, dim, input_length=40, trainable=True)
model_cnn_03.add(e)
model_cnn_03.add(Conv1D(filters=256, kernel_size=3, padding='valid', activation='relu', strides=1))
model_cnn_03.add(GlobalMaxPooling1D())
model_cnn_03.add(Dense(64, activation='relu'))
#model_cnn_03.add(Dense(64, activation='relu'))
model_cnn_03.add(Dense(3, activation='softmax'))
model_cnn_03.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model_cnn_03.summary())

t0 = time()
history = model_cnn_03.fit(x_train_seq, y_train, validation_data=(x_val_seq, y_val), epochs=20, batch_size=32, verbose=2)
print ("-" * 80)

import matplotlib.pyplot as plt
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

score = model_cnn_03.evaluate(x=x_val_seq, y=y_val) # [val_loss, val_acc]
pred = []
for x in range(len(x_val_seq)):
    result = model_cnn_03.predict(x_val_seq[x].reshape(1, x_val_seq.shape[1]), verbose=2)
    p = np.argmax(result)
    pred.append(p)

prec = precision_score(y_val,pred, average=None)
rec = recall_score(y_val,pred, average=None)
f1 = f1_score(y_val,pred, average=None)
train_test_time = time() - t0

print ("\n")
print("Best validation [loss,accuracy] :")
print(score)
print("Train and test time: {0} sec".format(train_test_time))

print ("\n")
print("Precision [Sadness, Joy]: ")
print(prec)

print ("\n")
print("Recall [Sadness, Joy]: ")
print(rec)

print ("\n")
print("F1_score [Sadness, Joy]: ")
print(f1)

'''
Wiki-news pre-trained w_v (filters = 200, kernel = 2, dense = 64, trainable)
Best validation [loss,accuracy] :
[0.16411415137512042, 0.9365981138506988]

Precision [Sadness, Joy]: 
[ 0.95597228  0.83772538]

Recall [Sadness, Joy]: 
[ 0.9678085   0.78851175]

F1_score [Sadness, Joy]: 
[ 0.96185398  0.81237391]

'''
