Layer (type)                 Output Shape              Param #   
=================================================================
embedding_1 (Embedding)      (None, 40, 200)           9053800   
_________________________________________________________________
spatial_dropout1d_1 (Spatial (None, 40, 200)           0         
_________________________________________________________________
lstm_1 (LSTM)                (None, 128)               168448    
_________________________________________________________________
dense_1 (Dense)              (None, 64)                8256      
_________________________________________________________________
dense_2 (Dense)              (None, 3)                 195       
=================================================================
Total params: 9,230,699
Trainable params: 176,899
Non-trainable params: 9,053,800
_________________________________________________________________
None
Train on 95884 samples, validate on 23971 samples
Epoch 1/20
 - 291s - loss: 0.7391 - acc: 0.6813 - val_loss: 0.6874 - val_acc: 0.7057
Epoch 2/20
 - 290s - loss: 0.6948 - acc: 0.7019 - val_loss: 0.6809 - val_acc: 0.7064
Epoch 3/20
 - 291s - loss: 0.6703 - acc: 0.7130 - val_loss: 0.6450 - val_acc: 0.7247
Epoch 4/20
 - 291s - loss: 0.6540 - acc: 0.7208 - val_loss: 0.6449 - val_acc: 0.7235
Epoch 5/20
 - 296s - loss: 0.6378 - acc: 0.7280 - val_loss: 0.6148 - val_acc: 0.7351
Epoch 6/20
 - 293s - loss: 0.6218 - acc: 0.7330 - val_loss: 0.6046 - val_acc: 0.7351
Epoch 7/20
 - 293s - loss: 0.6073 - acc: 0.7404 - val_loss: 0.5857 - val_acc: 0.7477
Epoch 8/20
 - 293s - loss: 0.5939 - acc: 0.7446 - val_loss: 0.5723 - val_acc: 0.7544
Epoch 9/20
 - 293s - loss: 0.5783 - acc: 0.7527 - val_loss: 0.5587 - val_acc: 0.7615
Epoch 10/20
 - 254s - loss: 0.5657 - acc: 0.7560 - val_loss: 0.5458 - val_acc: 0.7670
Epoch 11/20
 - 254s - loss: 0.5532 - acc: 0.7635 - val_loss: 0.5343 - val_acc: 0.7708
Epoch 12/20
 - 254s - loss: 0.5414 - acc: 0.7687 - val_loss: 0.5209 - val_acc: 0.7768
Epoch 13/20
 - 254s - loss: 0.5324 - acc: 0.7717 - val_loss: 0.5176 - val_acc: 0.7773
Epoch 14/20
 - 256s - loss: 0.5229 - acc: 0.7756 - val_loss: 0.5113 - val_acc: 0.7826
Epoch 15/20
 - 256s - loss: 0.5164 - acc: 0.7802 - val_loss: 0.5040 - val_acc: 0.7861
Epoch 16/20
 - 255s - loss: 0.5112 - acc: 0.7818 - val_loss: 0.5040 - val_acc: 0.7841
Epoch 17/20
 - 254s - loss: 0.5028 - acc: 0.7857 - val_loss: 0.4967 - val_acc: 0.7869
Epoch 18/20
 - 255s - loss: 0.4996 - acc: 0.7872 - val_loss: 0.4955 - val_acc: 0.7907
Epoch 19/20
 - 255s - loss: 0.4926 - acc: 0.7896 - val_loss: 0.4959 - val_acc: 0.7890
Epoch 20/20
 - 256s - loss: 0.4869 - acc: 0.7927 - val_loss: 0.4864 - val_acc: 0.7940
--------------------------------------------------------------------------------
