45.000f with stop words
accuracy score: 80.74%
--------------------------------------------------------------------------------
Confusion Matrix
          predicted_negative        ...          predicted_neutrar
negative                5084        ...                       2193
positive                 233        ...                        656
neutral                 1380        ...                      13629
[3 rows x 3 columns]
--------------------------------------------------------------------------------
Classification Report
              precision    recall  f1-score   support
    negative       0.76      0.69      0.72      7338
    positive       0.80      0.42      0.55      1529
     neutral       0.83      0.90      0.86     15104

   micro avg       0.81      0.81      0.81     23971
   macro avg       0.80      0.67      0.71     23971
weighted avg       0.80      0.81      0.80     23971