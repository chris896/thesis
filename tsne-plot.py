import pandas as pd
pd.options.mode.chained_assignment = None
import numpy as np
import re
import nltk
from gensim.models import word2vec
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn import neighbors, datasets
from sklearn.decomposition import PCA
from keras import backend as K
SEED = 2000

# Create color maps
cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA'])
cmap_bold = ListedColormap(['#FF0000', '#00FF00'])

n_samples = 2000
## Get weights
#conv_embds = model_conv.layers[6].get_weights()[0]
get_layer_output = K.function([loaded_CNN_model.layers[0].input],
                                  [loaded_CNN_model.layers[6].output])

x_train_layer_output = get_layer_output([x_train_seq[:n_samples]])[0]
x_val_layer_output = get_layer_output([x_val_seq[:n_samples]])[0]
y_train = y_train[:n_samples]
y_val = y_val[:n_samples]

knn = KNeighborsClassifier(n_neighbors=1, metric='cosine')
knn.fit(x_train_layer_output, y_train)
#distances, idx = nn.kneighbors(x_val_layer_output)
#idx = idx[0]

y_pred= knn.predict(x_val_layer_output).tolist()
print(accuracy_score(y_val,y_pred))



## ~~~~~~~~~~~ Visualize layers embeddings in 2-D space ~~~~~~~~~~~~~ ##

def tsne_plot(labels, embeddings):

    tsne_model = TSNE(perplexity=30, n_components=2, init='pca', n_iter=2500, random_state=SEED)
    new_values = tsne_model.fit_transform(embeddings)

    x = []
    y = []
    for value in new_values:
        x.append(value[0])
        y.append(value[1])


    plt.figure()
    plt.scatter(x, y, c=labels, cmap=cmap_bold)
    plt.xlim(min(x), max(x))
    plt.ylim(min(y), max(y))
    plt.title("t-sne Representation")
    plt.show()



tsne_plot(y_train.values, x_train_layer_output)
tsne_plot(y_val.values, x_val_layer_output)
tsne_plot(y_pred, x_val_layer_output)



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PCA-2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##

pca2 = PCA(n_components=2)
x_train_layer_output_pca_2 = pca2.fit_transform(x_train_layer_output)
x_val_layer_output_pca_2 = pca2.fit_transform(x_val_layer_output)

# kNeighbors classifier on train_pca-2 set
knn = KNeighborsClassifier(n_neighbors=3, algorithm='brute', metric='cosine', weights='distance')
knn.fit(x_train_layer_output_pca_2, y_train)
y_pred = knn.predict(x_val_layer_output_pca_2).tolist()
print(accuracy_score(y_val,y_pred))


h = 2
# calculate min, max and limits
x_min, x_max = x_train_layer_output_pca_2[:, 0].min() - 1, x_train_layer_output_pca_2[:, 0].max() + 1
y_min, y_max = x_train_layer_output_pca_2[:, 1].min() - 1, x_train_layer_output_pca_2[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))

# predict class using data and kNN classifier
Z = knn.predict(np.c_[xx.ravel(), yy.ravel()])

# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.figure()
plt.pcolormesh(xx, yy, Z, cmap=cmap_light)

# Plot also the training points
plt.scatter(x_val_layer_output_pca_2[:, 0], x_val_layer_output_pca_2[:, 1], c=y_pred, cmap=cmap_bold)
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.title("2-Class classification (k = %i)" % 3)
plt.show()




